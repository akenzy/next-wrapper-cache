import Redis from 'ioredis';
import { GetServerSidePropsContext } from 'next';
export declare class WrapperNextCache {
    protected clientRedis: Redis | null;
    constructor({ host, port }: {
        host: string;
        port: number;
    });
    private create;
    protected isConnectRedis(): boolean;
    GetServerSideProps(handler: (context: any) => Promise<any>, ttl?: number): (context: GetServerSidePropsContext) => Promise<any>;
}
//# sourceMappingURL=index.d.ts.map