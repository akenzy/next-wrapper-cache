import Redis from 'ioredis';
import { GetServerSidePropsContext } from 'next';

export class WrapperNextCache {
	protected clientRedis: Redis | null;

	constructor({ host, port }: { host: string; port: number }) {
		this.clientRedis = this.create({ host, port });
	}

	private create({ host, port }: { host: string; port: number }) {
		return new Redis({
			host,
			port
		});
	}

	protected isConnectRedis() {
		return this.clientRedis?.status === 'ready';
	}

	GetServerSideProps(handler: (context: any) => Promise<any>, ttl?: number) {
		const _ttl: number = ttl || 30;
		return async (context: GetServerSidePropsContext) => {
			const url = context.resolvedUrl;
			if (this.isConnectRedis()) {
				const cache = await this.clientRedis?.get(url);
				if (cache) {
					return JSON.parse(cache);
				}
			}
			const result = await handler(context);
			if (this.isConnectRedis()) {
				const cacheValue = JSON.stringify(result);
				console.log('cacheValue', cacheValue);
				await this.clientRedis?.setex(url, _ttl, cacheValue, (err, res) => {
					console.log('err', err);
					console.log('res', res);
				});
			}

			return result;
		};
	}
}
